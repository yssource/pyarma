// Copyright 2020-2021 Jason Rumengan
// Copyright 2020-2021 Data61/CSIRO
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include "pybind11/pybind11.h"
#include "pybind11/operators.h"
#include "armadillo"

namespace py = pybind11;

namespace pyarma {
    // Expose SizeMat, the class used by size(X)
    void expose_size(py::module &m) {
        using Class = arma::SizeMat;

        py::class_<Class>(m, "size_mat")

            // Expose arithmetic operations
            .def(py::self + py::self)
            .def(py::self - py::self)
            .def(py::self + arma::uword())
            .def(py::self - arma::uword())
            .def(py::self * arma::uword())
            .def(py::self / arma::uword())
            .def(py::self == py::self)
            .def(py::self != py::self)

            // Overloads str() and repr()
            .def("__repr__", [](const Class &size) {
                std::ostringstream stream;
                stream << size << std::endl;
                return stream.str();
            });
    }
}