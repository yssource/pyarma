from pyarma import *

from pyarma import *

A = mat(2,3,fill_randu)
Af = fmat(2,3,fill_randu)
Ac = cx_mat(2,3,fill_randu)
Acf = cx_fmat(2,3,fill_randu)
Au = umat(2,3,fill_randu)
Ai = imat(2,3,fill_randu)

B = mat(3,4,fill_randu)
Bf = fmat(3,4,fill_randu)
Bc = cx_mat(3,4,fill_randu)
Bcf = cx_fmat(3,4,fill_randu)
Bu = umat(3,4,fill_randu)
Bi = imat(3,4,fill_randu)

C = cube(2,3,4,fill_randu)
Cf = fcube(2,3,4,fill_randu)
Cc = cx_cube(2,3,4,fill_randu)
Ccf = cx_fcube(2,3,4,fill_randu)
Cu = ucube(2,3,4,fill_randu)
Ci = icube(2,3,4,fill_randu)

D = cube(3,4,5,fill_randu)
Df = fcube(3,4,5,fill_randu)
Dc = cx_cube(3,4,5,fill_randu)
Dcf = cx_fcube(3,4,5,fill_randu)
Du = ucube(3,4,5,fill_randu)
Di = icube(3,4,5,fill_randu)

accu(C)
sum(C)
sum(sum(C))
sum(sum(sum(C)))
abs(C)
min(C)
min(min(C))
min(min(min(C)))
max(C)
max(max(C))
max(max(max(C)))
mean(C)
mean(mean(C))
mean(mean(mean(C)))
as_scalar(sum(sum(sum(C))))
as_scalar(min(min(min(C))))
as_scalar(max(max(max(C))))
as_scalar(mean(mean(mean(C))))

real(C)
imag(C)
square(C)
sqrt(C)

join_slices(C,C)
join_slices(C,A)

find(C<0.5)
find_finite(C)
find_nonfinite(C)
find_unique(C)
index_min(C)
index_max(C)
reshape(C, 3, 4, 5)
reshape(C, size(D))
resize(C, 3, 4, 5)
resize(C, size(D))
size(C)

pow(C,0)
pow(C,0.5)
pow(C,1)
pow(C,2)
round(C)
floor(C)
ceil(C)
trunc(C)
sign(C)

log(C)
log2(C)
log10(C)
log1p(C)
trunc_log(C)
exp(C)
exp2(C)
exp10(C)
expm1(C)
trunc_exp(C)
arg(C)
erf(C)
erfc(C)
lgamma(C)
tgamma(C)
cos(C)
acos(C)
cosh(C)
acosh(C)
sin(C)
asin(C)
sinh(C)
asinh(C)
tan(C)
atan(C)
tanh(C)
atanh(C)
sinc(C)
atan2(C, C)
hypot(C, C)

clamp(C, 0.1, 0.9)
vectorise(C)
fcube(C)
cx_cube(C)
cx_fcube(C)
ucube(C)
icube(C)
approx_equal(C, C, "absdiff", 0.1)

#float

accu(Cf)
sum(Cf)
sum(sum(Cf))
sum(sum(sum(Cf)))
abs(Cf)
min(Cf)
min(min(Cf))
min(min(min(Cf)))
max(Cf)
max(max(Cf))
max(max(max(Cf)))
mean(Cf)
mean(mean(Cf))
mean(mean(mean(Cf)))
as_scalar(sum(sum(sum(Cf))))
as_scalar(min(min(min(Cf))))
as_scalar(max(max(max(Cf))))
as_scalar(mean(mean(mean(Cf))))

real(Cf)
imag(Cf)
square(Cf)
sqrt(Cf)

join_slices(Cf,Cf)
join_slices(Cf,Af)

find(Cf<0.5)
find_finite(Cf)
find_nonfinite(Cf)
find_unique(Cf)
index_min(Cf)
index_max(Cf)
reshape(Cf, 3, 4, 5)
reshape(Cf, size(Df))
resize(Cf, 3, 4, 5)
resize(Cf, size(Df))
size(Cf)

pow(Cf,0)
pow(Cf,0.5)
pow(Cf,1)
pow(Cf,2)
round(Cf)
floor(Cf)
ceil(Cf)
trunc(Cf)
sign(Cf)

log(Cf)
log2(Cf)
log10(Cf)
log1p(Cf)
trunc_log(Cf)
exp(Cf)
exp2(Cf)
exp10(Cf)
expm1(Cf)
trunc_exp(Cf)
arg(Cf)
erf(Cf)
erfc(Cf)
lgamma(Cf)
tgamma(Cf)
cos(Cf)
acos(Cf)
cosh(Cf)
acosh(Cf)
sin(Cf)
asin(Cf)
sinh(Cf)
asinh(Cf)
tan(Cf)
atan(Cf)
tanh(Cf)
atanh(Cf)
sinc(Cf)
atan2(Cf, Cf)
hypot(Cf, Cf)

clamp(Cf, 0.1, 0.9)
vectorise(Cf)
fcube(Cf)
cx_cube(Cf)
cx_fcube(Cf)
ucube(Cf)
icube(Cf)
approx_equal(Cf, Cf, "absdiff", 0.1)

# cx
accu(Cc)
sum(Cc)
sum(sum(Cc))
sum(sum(sum(Cc)))
abs(Cc)
min(Cc)
min(min(Cc))
min(min(min(Cc)))
max(Cc)
max(max(Cc))
max(max(max(Cc)))
mean(Cc)
mean(mean(Cc))
mean(mean(mean(Cc)))
as_scalar(sum(sum(sum(Cc))))
as_scalar(min(min(min(Cc))))
as_scalar(max(max(max(Cc))))
as_scalar(mean(mean(mean(Cc))))

real(Cc)
imag(Cc)
conj(Cc)
square(Cc)
sqrt(Cc)

join_slices(Cc,Cc)
join_slices(Cc,Ac)

find(Cc<0.5)
find_finite(Cc)
find_nonfinite(Cc)
find_unique(Cc)
index_min(Cc)
index_max(Cc)
reshape(Cc, 3, 4, 5)
reshape(Cc, size(Dc))
resize(Cc, 3, 4, 5)
resize(Cc, size(Dc))
size(Cc)

pow(Cc,0)
pow(Cc,0.5)
pow(Cc,1)
pow(Cc,2)
round(Cc)
floor(Cc)
ceil(Cc)
trunc(Cc)
sign(Cc)

log(Cc)
log2(Cc)
log10(Cc)
log1p(Cc)
trunc_log(Cc)
exp(Cc)
exp2(Cc)
exp10(Cc)
expm1(Cc)
trunc_exp(Cc)
arg(Cc)
erf(Cc)
erfc(Cc)
lgamma(Cc)
tgamma(Cc)
cos(Cc)
acos(Cc)
cosh(Cc)
acosh(Cc)
sin(Cc)
asin(Cc)
sinh(Cc)
asinh(Cc)
tan(Cc)
atan(Cc)
tanh(Cc)
atanh(Cc)
sinc(Cc)
atan2(Cc, Cc)
hypot(Cc, Cc)

clamp(Cc, 0.1, 0.9)
vectorise(Cc)
fcube(Cc)
cx_cube(Cc)
cx_fcube(Cc)
ucube(Cc)
icube(Cc)
approx_equal(Cc, Cc, "absdiff", 0.1)

# cxf

accu(Ccf)
sum(Ccf)
sum(sum(Ccf))
sum(sum(sum(Ccf)))
abs(Ccf)
min(Ccf)
min(min(Ccf))
min(min(min(Ccf)))
max(Ccf)
max(max(Ccf))
max(max(max(Ccf)))
mean(Ccf)
mean(mean(Ccf))
mean(mean(mean(Ccf)))
as_scalar(sum(sum(sum(Ccf))))
as_scalar(min(min(min(Ccf))))
as_scalar(max(max(max(Ccf))))
as_scalar(mean(mean(mean(Ccf))))

real(Ccf)
imag(Ccf)
conj(Ccf)
square(Ccf)
sqrt(Ccf)

join_slices(Ccf,Ccf)
join_slices(Ccf,Acf)

find(Ccf<0.5)
find_finite(Ccf)
find_nonfinite(Ccf)
find_unique(Ccf)
index_min(Ccf)
index_max(Ccf)
reshape(Ccf, 3, 4, 5)
reshape(Ccf, size(Dcf))
resize(Ccf, 3, 4, 5)
resize(Ccf, size(Dcf))
size(Ccf)

pow(Ccf,0)
pow(Ccf,0.5)
pow(Ccf,1)
pow(Ccf,2)
round(Ccf)
floor(Ccf)
ceil(Ccf)
trunc(Ccf)
sign(Ccf)

log(Ccf)
log2(Ccf)
log10(Ccf)
log1p(Ccf)
trunc_log(Ccf)
exp(Ccf)
exp2(Ccf)
exp10(Ccf)
expm1(Ccf)
trunc_exp(Ccf)
arg(Ccf)
erf(Ccf)
erfc(Ccf)
lgamma(Ccf)
tgamma(Ccf)
cos(Ccf)
acos(Ccf)
cosh(Ccf)
acosh(Ccf)
sin(Ccf)
asin(Ccf)
sinh(Ccf)
asinh(Ccf)
tan(Ccf)
atan(Ccf)
tanh(Ccf)
atanh(Ccf)
sinc(Ccf)
atan2(Ccf, Ccf)
hypot(Ccf, Ccf)

clamp(Ccf, 0.1, 0.9)
vectorise(Ccf)
fcube(Ccf)
cx_cube(Ccf)
cx_fcube(Ccf)
ucube(Ccf)
icube(Ccf)
approx_equal(Ccf, Ccf, "absdiff", 0.1)

#u
accu(Cu)
sum(Cu)
sum(sum(Cu))
sum(sum(sum(Cu)))
abs(Cu)
min(Cu)
min(min(Cu))
min(min(min(Cu)))
max(Cu)
max(max(Cu))
max(max(max(Cu)))
mean(Cu)
mean(mean(Cu))
mean(mean(mean(Cu)))
as_scalar(sum(sum(sum(Cu))))
as_scalar(min(min(min(Cu))))
as_scalar(max(max(max(Cu))))
as_scalar(mean(mean(mean(Cu))))

real(Cu)
imag(Cu)
square(Cu)
sqrt(Cu)

join_slices(Cu,Cu)
join_slices(Cu,Au)

find(Cu<0.5)
find_finite(Cu)
find_nonfinite(Cu)
find_unique(Cu)
index_min(Cu)
index_max(Cu)
reshape(Cu, 3, 4, 5)
reshape(Cu, size(Du))
resize(Cu, 3, 4, 5)
resize(Cu, size(Du))
size(Cu)

pow(Cu,0)
pow(Cu,0.5)
pow(Cu,1)
pow(Cu,2)
round(Cu)
floor(Cu)
ceil(Cu)
trunc(Cu)
sign(Cu)

log(Cu)
log2(Cu)
log10(Cu)
log1p(Cu)
trunc_log(Cu)
exp(Cu)
exp2(Cu)
exp10(Cu)
expm1(Cu)
trunc_exp(Cu)
arg(Cu)
erf(Cu)
erfc(Cu)
lgamma(Cu)
tgamma(Cu)
cos(Cu)
acos(Cu)
cosh(Cu)
acosh(Cu)
sin(Cu)
asin(Cu)
sinh(Cu)
asinh(Cu)
tan(Cu)
atan(Cu)
tanh(Cu)
atanh(Cu)
sinc(Cu)
atan2(Cu, Cu)
hypot(Cu, Cu)

clamp(Cu, 0.1, 0.9)
vectorise(Cu)
fcube(Cu)
cx_cube(Cu)
cx_fcube(Cu)
ucube(Cu)
icube(Cu)
approx_equal(Cu, Cu, "absdiff", 0.1)

#i
accu(Ci)
sum(Ci)
sum(sum(Ci))
sum(sum(sum(Ci)))
abs(Ci)
min(Ci)
min(min(Ci))
min(min(min(Ci)))
max(Ci)
max(max(Ci))
max(max(max(Ci)))
mean(Ci)
mean(mean(Ci))
mean(mean(mean(Ci)))
as_scalar(sum(sum(sum(Ci))))
as_scalar(min(min(min(Ci))))
as_scalar(max(max(max(Ci))))
as_scalar(mean(mean(mean(Ci))))

real(Ci)
imag(Ci)
square(Ci)
sqrt(Ci)

join_slices(Ci,Ci)
join_slices(Ci,Ai)

find(Ci<0.5)
find_finite(Ci)
find_nonfinite(Ci)
find_unique(Ci)
index_min(Ci)
index_max(Ci)
reshape(Ci, 3, 4, 5)
reshape(Ci, size(Dc))
resize(Ci, 3, 4, 5)
resize(Ci, size(Dc))
size(Ci)

pow(Ci,0)
pow(Ci,0.5)
pow(Ci,1)
pow(Ci,2)
round(Ci)
floor(Ci)
ceil(Ci)
trunc(Ci)
sign(Ci)

log(Ci)
log2(Ci)
log10(Ci)
log1p(Ci)
trunc_log(Ci)
exp(Ci)
exp2(Ci)
exp10(Ci)
expm1(Ci)
trunc_exp(Ci)
arg(Ci)
erf(Ci)
erfc(Ci)
lgamma(Ci)
tgamma(Ci)
cos(Ci)
acos(Ci)
cosh(Ci)
acosh(Ci)
sin(Ci)
asin(Ci)
sinh(Ci)
asinh(Ci)
tan(Ci)
atan(Ci)
tanh(Ci)
atanh(Ci)
sinc(Ci)
atan2(Ci, Ci)
hypot(Ci, Ci)

clamp(Ci, 0.1, 0.9)
vectorise(Ci)
fcube(Ci)
cx_cube(Ci)
cx_fcube(Ci)
ucube(Ci)
icube(Ci)
approx_equal(Ci, Ci, "absdiff", 0.1)

# mat
accu(A)
sum(A)
sum(sum(A))
sum(sum(sum(A)))
abs(A)
min(A)
min(min(A))
min(min(min(A)))
max(A)
max(max(A))
max(max(max(A)))
mean(A)
mean(mean(A))
mean(mean(mean(A)))
as_scalar(sum(sum(sum(A))))
as_scalar(min(min(min(A))))
as_scalar(max(max(max(A))))
as_scalar(mean(mean(mean(A))))

real(A)
imag(A)
square(A)
sqrt(A)

join_rows(A,A)
join_cols(A,A)

find(A<0.5)
find_finite(A)
find_nonfinite(A)
find_unique(A)
index_min(A)
index_max(A)
reshape(A, 3, 4, 5)
reshape(A, size(B))
resize(A, 3, 4, 5)
resize(A, size(B))
size(A)

pow(A,0)
pow(A,0.5)
pow(A,1)
pow(A,2)
round(A)
floor(A)
ceil(A)
trunc(A)
sign(A)

log(A)
log2(A)
log10(A)
log1p(A)
trunc_log(A)
exp(A)
exp2(A)
exp10(A)
expm1(A)
trunc_exp(A)
arg(A)
erf(A)
erfc(A)
lgamma(A)
tgamma(A)
cos(A)
acos(A)
cosh(A)
acosh(A)
sin(A)
asin(A)
sinh(A)
asinh(A)
tan(A)
atan(A)
tanh(A)
atanh(A)
sinc(A)
atan2(A, A)
hypot(A, A)

clamp(A, 0.1, 0.9)
vectorise(A)
fcube(A)
cx_cube(A)
cx_fcube(A)
ucube(A)
icube(A)
approx_equal(A, A, "absdiff", 0.1)

#float

accu(Af)
sum(Af)
sum(sum(Af))
sum(sum(sum(Af)))
abs(Af)
min(Af)
min(min(Af))
min(min(min(Af)))
max(Af)
max(max(Af))
max(max(max(Af)))
mean(Af)
mean(mean(Af))
mean(mean(mean(Af)))
as_scalar(sum(sum(sum(Af))))
as_scalar(min(min(min(Af))))
as_scalar(max(max(max(Af))))
as_scalar(mean(mean(mean(Af))))

real(Af)
imag(Af)
square(Af)
sqrt(Af)

join_rows(Af,Af)
join_cols(Af,Af)

find(Af<0.5)
find_finite(Af)
find_nonfinite(Af)
find_unique(Af)
index_min(Af)
index_max(Af)
reshape(Af, 3, 4, 5)
reshape(Af, size(Bf))
resize(Af, 3, 4, 5)
resize(Af, size(Bf))
size(Af)

pow(Af,0)
pow(Af,0.5)
pow(Af,1)
pow(Af,2)
round(Af)
floor(Af)
ceil(Af)
trunc(Af)
sign(Af)

log(Af)
log2(Af)
log10(Af)
log1p(Af)
trunc_log(Af)
exp(Af)
exp2(Af)
exp10(Af)
expm1(Af)
trunc_exp(Af)
arg(Af)
erf(Af)
erfc(Af)
lgamma(Af)
tgamma(Af)
cos(Af)
acos(Af)
cosh(Af)
acosh(Af)
sin(Af)
asin(Af)
sinh(Af)
asinh(Af)
tan(Af)
atan(Af)
tanh(Af)
atanh(Af)
sinc(Af)
atan2(Af, Af)
hypot(Af, Af)

clamp(Af, 0.1, 0.9)
vectorise(Af)
fcube(Af)
cx_cube(Af)
cx_fcube(Af)
ucube(Af)
icube(Af)
approx_equal(Af, Af, "absdiff", 0.1)

# cx
accu(Ac)
sum(Ac)
sum(sum(Ac))
sum(sum(sum(Ac)))
abs(Ac)
min(Ac)
min(min(Ac))
min(min(min(Ac)))
max(Ac)
max(max(Ac))
max(max(max(Ac)))
mean(Ac)
mean(mean(Ac))
mean(mean(mean(Ac)))
as_scalar(sum(sum(sum(Ac))))
as_scalar(min(min(min(Ac))))
as_scalar(max(max(max(Ac))))
as_scalar(mean(mean(mean(Ac))))

real(Ac)
imag(Ac)
conj(Ac)
square(Ac)
sqrt(Ac)

join_rows(Ac,Ac)
join_cols(Ac,Ac)

find(Ac<0.5)
find_finite(Ac)
find_nonfinite(Ac)
find_unique(Ac)
index_min(Ac)
index_max(Ac)
reshape(Ac, 3, 4, 5)
reshape(Ac, size(Bc))
resize(Ac, 3, 4, 5)
resize(Ac, size(Bc))
size(Ac)

pow(Ac,0)
pow(Ac,0.5)
pow(Ac,1)
pow(Ac,2)
round(Ac)
floor(Ac)
ceil(Ac)
trunc(Ac)
sign(Ac)

log(Ac)
log2(Ac)
log10(Ac)
log1p(Ac)
trunc_log(Ac)
exp(Ac)
exp2(Ac)
exp10(Ac)
expm1(Ac)
trunc_exp(Ac)
arg(Ac)
erf(Ac)
erfc(Ac)
lgamma(Ac)
tgamma(Ac)
cos(Ac)
acos(Ac)
cosh(Ac)
acosh(Ac)
sin(Ac)
asin(Ac)
sinh(Ac)
asinh(Ac)
tan(Ac)
atan(Ac)
tanh(Ac)
atanh(Ac)
sinc(Ac)
atan2(Ac, Ac)
hypot(Ac, Ac)

clamp(Ac, 0.1, 0.9)
vectorise(Ac)
fcube(Ac)
cx_cube(Ac)
cx_fcube(Ac)
ucube(Ac)
icube(Ac)
approx_equal(Ac, Ac, "absdiff", 0.1)

# cxf

accu(Acf)
sum(Acf)
sum(sum(Acf))
sum(sum(sum(Acf)))
abs(Acf)
min(Acf)
min(min(Acf))
min(min(min(Acf)))
max(Acf)
max(max(Acf))
max(max(max(Acf)))
mean(Acf)
mean(mean(Acf))
mean(mean(mean(Acf)))
as_scalar(sum(sum(sum(Acf))))
as_scalar(min(min(min(Acf))))
as_scalar(max(max(max(Acf))))
as_scalar(mean(mean(mean(Acf))))

real(Acf)
imag(Acf)
conj(Acf)
square(Acf)
sqrt(Acf)

join_rows(Acf,Acf)
join_cols(Acf,Acf)

find(Acf<0.5)
find_finite(Acf)
find_nonfinite(Acf)
find_unique(Acf)
index_min(Acf)
index_max(Acf)
reshape(Acf, 3, 4, 5)
reshape(Acf, size(Bcf))
resize(Acf, 3, 4, 5)
resize(Acf, size(Bcf))
size(Acf)

pow(Acf,0)
pow(Acf,0.5)
pow(Acf,1)
pow(Acf,2)
round(Acf)
floor(Acf)
ceil(Acf)
trunc(Acf)
sign(Acf)

log(Acf)
log2(Acf)
log10(Acf)
log1p(Acf)
trunc_log(Acf)
exp(Acf)
exp2(Acf)
exp10(Acf)
expm1(Acf)
trunc_exp(Acf)
arg(Acf)
erf(Acf)
erfc(Acf)
lgamma(Acf)
tgamma(Acf)
cos(Acf)
acos(Acf)
cosh(Acf)
acosh(Acf)
sin(Acf)
asin(Acf)
sinh(Acf)
asinh(Acf)
tan(Acf)
atan(Acf)
tanh(Acf)
atanh(Acf)
sinc(Acf)
atan2(Acf, Acf)
hypot(Acf, Acf)

clamp(Acf, 0.1, 0.9)
vectorise(Acf)
fcube(Acf)
cx_cube(Acf)
cx_fcube(Acf)
ucube(Acf)
icube(Acf)
approx_equal(Acf, Acf, "absdiff", 0.1)

#u
accu(Au)
sum(Au)
sum(sum(Au))
sum(sum(sum(Au)))
abs(Au)
min(Au)
min(min(Au))
min(min(min(Au)))
max(Au)
max(max(Au))
max(max(max(Au)))
mean(Au)
mean(mean(Au))
mean(mean(mean(Au)))
as_scalar(sum(sum(sum(Au))))
as_scalar(min(min(min(Au))))
as_scalar(max(max(max(Au))))
as_scalar(mean(mean(mean(Au))))

real(Au)
imag(Au)
square(Au)
sqrt(Au)

join_rows(Au,Au)
join_cols(Au,Au)

find(Au<0.5)
find_finite(Au)
find_nonfinite(Au)
find_unique(Au)
index_min(Au)
index_max(Au)
reshape(Au, 3, 4, 5)
reshape(Au, size(Bu))
resize(Au, 3, 4, 5)
resize(Au, size(Bu))
size(Au)

pow(Au,0)
pow(Au,0.5)
pow(Au,1)
pow(Au,2)
round(Au)
floor(Au)
ceil(Au)
trunc(Au)
sign(Au)

log(Au)
log2(Au)
log10(Au)
log1p(Au)
trunc_log(Au)
exp(Au)
exp2(Au)
exp10(Au)
expm1(Au)
trunc_exp(Au)
arg(Au)
erf(Au)
erfc(Au)
lgamma(Au)
tgamma(Au)
cos(Au)
acos(Au)
cosh(Au)
acosh(Au)
sin(Au)
asin(Au)
sinh(Au)
asinh(Au)
tan(Au)
atan(Au)
tanh(Au)
atanh(Au)
sinc(Au)
atan2(Au, Au)
hypot(Au, Au)

clamp(Au, 0.1, 0.9)
vectorise(Au)
fcube(Au)
cx_cube(Au)
cx_fcube(Au)
ucube(Au)
icube(Au)
approx_equal(Au, Au, "absdiff", 0.1)

#i
accu(Ai)
sum(Ai)
sum(sum(Ai))
sum(sum(sum(Ai)))
abs(Ai)
min(Ai)
min(min(Ai))
min(min(min(Ai)))
max(Ai)
max(max(Ai))
max(max(max(Ai)))
mean(Ai)
mean(mean(Ai))
mean(mean(mean(Ai)))
as_scalar(sum(sum(sum(Ai))))
as_scalar(min(min(min(Ai))))
as_scalar(max(max(max(Ai))))
as_scalar(mean(mean(mean(Ai))))

real(Ai)
imag(Ai)
square(Ai)
sqrt(Ai)

join_rows(Ai,Ai)
join_cols(Ai,Ai)

find(Ai<0.5)
find_finite(Ai)
find_nonfinite(Ai)
find_unique(Ai)
index_min(Ai)
index_max(Ai)
reshape(Ai, 3, 4, 5)
reshape(Ai, size(Bi))
resize(Ai, 3, 4, 5)
resize(Ai, size(Bi))
size(Ai)

pow(Ai,0)
pow(Ai,0.5)
pow(Ai,1)
pow(Ai,2)
round(Ai)
floor(Ai)
ceil(Ai)
trunc(Ai)
sign(Ai)

log(Ai)
log2(Ai)
log10(Ai)
log1p(Ai)
trunc_log(Ai)
exp(Ai)
exp2(Ai)
exp10(Ai)
expm1(Ai)
trunc_exp(Ai)
arg(Ai)
erf(Ai)
erfc(Ai)
lgamma(Ai)
tgamma(Ai)
cos(Ai)
acos(Ai)
cosh(Ai)
acosh(Ai)
sin(Ai)
asin(Ai)
sinh(Ai)
asinh(Ai)
tan(Ai)
atan(Ai)
tanh(Ai)
atanh(Ai)
sinc(Ai)
atan2(Ai, Ai)
hypot(Ai, Ai)

clamp(Ai, 0.1, 0.9)
vectorise(Ai)
fcube(Ai)
cx_cube(Ai)
cx_fcube(Ai)
ucube(Ai)
icube(Ai)
approx_equal(Ai, Ai, "absdiff", 0.1)