# Copyright 2020 Terry Yue Zhuo
# Copyright 2020 Data61/CSIRO

# Licensed under the Apache License, Version 2.0 (the "License"
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------

import pytest
import pyarma as pa

def test_mat_attributes():

    A = pa.mat(5,6, pa.fill_zeros)
    A[0,0] = 1.0
    A[A.n_rows-1,A.n_cols-1] = 1.0
    assert A.n_rows == 5
    assert A.n_cols == 6
    assert A.n_elem == 30
    assert len(pa.nonzeros(A)) == 2
    assert all(isinstance(a, float) for a in iter(A))

def test_cx_mat_attributes():

    A = pa.cx_mat(5,6, pa.fill_zeros)
    A[0,0] = 1+1j
    A[A.n_rows-1,A.n_cols-1] = 1+1j
    assert A.n_rows == 5
    assert A.n_cols == 6
    assert A.n_elem == 30
    assert len(pa.nonzeros(A)) == 2
    assert all(isinstance(a, complex) for a in iter(A))

def test_umat_attributes():

    A = pa.umat(5,6, pa.fill_zeros)
    A[0,0] = 1
    A[A.n_rows-1,A.n_cols-1] = 1
    assert A.n_rows == 5
    assert A.n_cols == 6
    assert A.n_elem == 30
    assert len(pa.nonzeros(A)) == 2
    assert all(isinstance(a, int) for a in iter(A))

# vec B(5)
# assert B.n_rows == 5
# assert B.n_cols == 1
# assert B.n_elem == 5

# rowvec C(6)
# assert C.n_rows == 1
# assert C.n_cols == 6
# assert C.n_elem == 6

# D = cube(5,6,2)
# assert D.n_rows   == 5
# assert D.n_cols   == 6
# assert D.n_slices == 2
# assert D.n_elem   == 60

# E = sp_mat(50,60)
# E[0,0] = 1.0
# E[E.n_rows-1,E.n_cols-1] = 1.0

# assert E.n_rows    == 50
# assert E.n_cols    == 60
# assert E.n_elem    == 3000
# assert E.n_nonzero == 2
